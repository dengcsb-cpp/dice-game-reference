﻿#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

struct Roll
{
	int dice1;
	int dice2;
};

// int bet(int& gold) // You can deduct the player�s gold and return the bet as a normal variable.
// void bet(int& gold, int& bet) // You can deduct player�s gold and bet will be set via referencing.
int bet(int gold); // You cannot deduct player�s gold. Function is solely used for determining bet. This is the simplest and most intuitive.

// We need both dice to be referenced since we'll be updating both of them after this function call.
void rollDice(int& dice1, int& dice2);

// Notice that only gold is referenced. All the other variables will be read only.
void payout(int& gold, int bet, int pDice1, int pDice2, int eDice1, int eDice2);

// After a single round, gold will be updated to a new value
// We can make this to simply return the gold but it's vague. Return type is useful for function that has a clear output.
void playRound(int& gold);

int main()
{
	srand(time(0));

	int gold = 1000;
	while (gold > 0)
	{
		cout << "Gold: " << gold << endl;
		playRound(gold);
		system("pause");
		system("cls");
	}

	return 0;
}

int bet(int gold)
{
	cout << "Input bet: ";
	int value;
	cin >> value;
	return value;
}

void rollDice(int& dice1, int& dice2)
{
	dice1 = rand() % 6 + 1;
	dice2 = rand() % 6 + 1;
}

void payout(int& gold, int bet, int pDice1, int pDice2, int eDice1, int eDice2)
{
	int pValue = pDice1 + pDice2;
	int eValue = eDice1 + eDice2;

	if (pValue == eValue)
	{
		cout << "Draw!" << endl;
		return;
	}

	if (pValue > eValue)
	{
		gold += bet;
		cout << "Player won " << bet << endl;
	}
	else
	{
		gold -= bet;
		cout << "Player lost " << bet << endl;
	}
}

void playRound(int& gold)
{
	// Input bet
	int wager = bet(gold);

	// Roll dice for player
	int pDice1;
	int pDice2;
	rollDice(pDice1, pDice2);
	cout << "Player rolls " << pDice1 << " " << pDice2 << endl;

	// Roll dice for enemy
	int eDice1;
	int eDice2;
	rollDice(eDice1, eDice2);
	cout << "Enemy rolls " << eDice1 << " " << eDice2 << endl;

	payout(gold, wager, pDice1, pDice2, eDice1, eDice2);
}
